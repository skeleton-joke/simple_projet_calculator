const sum = require("./sum.js");

test('adds 1 + 2 to equal 3', () => {
    expect(sum(1, 2)).toBe(3);
  }
);

test("add 1 + 2 equals 3 with string", () => {
    expect(sum("1","2")).toBe(3); 
  }
);